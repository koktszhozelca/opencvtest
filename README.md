# How to run the code

```bash
# Make sure it is python 3.

python -m virtualenv venv
source venv/bin/activate
pip install -r requirement.txt
python index.py
```